" Colorscheme: Pure, dark variant
" Author:      pwseo (pwseo@posteo.org)
" Last Update: 8 Jun 2016

set background=dark

hi clear
if exists('syntax_on')
    syntax reset
endif

let g:colors_name = 'pure-dark'
