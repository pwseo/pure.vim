" Colorscheme: Pure, light variant
" Author:      pwseo (pwseo@posteo.org)
" Last Update: 8 Jun 2016

set background=light

hi clear
if exists('syntax_on')
    syntax reset
endif

let g:colors_name = 'pure-light'
